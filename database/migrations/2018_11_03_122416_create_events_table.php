<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('event_type_id');
            $table->unsignedInteger('venue_hall_id');
            $table->string('event_name')->nullable('false');
            $table->dateTime('event_date')->nullable('false');
            $table->unsignedSmallInteger('duration_in_minutes')->nullable('false');
            $table->timestamps();
            $table->foreign('event_type_id')->references('id')->on('event_types');
            $table->foreign('venue_hall_id')->references('id')->on('venue_halls');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
