<?php

require '../vendor/autoload.php';

$client = new MongoDB\Client("mongodb://localhost:27017");
$tagsCollection = $client->evently->tags;
$newTagsCollection = $client->evently->new_tags;

$result = $tagsCollection->find();

foreach ($result as $document) {
    $tag = $document->tag;

    foreach ($document->entities as $entity) {
        $entityType = $entity->type;

        foreach ($entity->ids as $entityId) {
            $newTagsCollection->updateOne(
                ['entityId' => $entityId, 'entityType' => $entityType],
                ['$addToSet' => ['tags' => $tag]],
                ['upsert' => true]
            );
        }
    }
}
