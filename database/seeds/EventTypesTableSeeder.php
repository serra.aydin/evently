<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventTypesTableSeeder extends Seeder
{
    public function run()
    {
        $eventTypes = ['concert', 'theatre', 'movie', 'dance'];

        foreach ($eventTypes as $eventType) {
            DB::table('event_types')->insert([
                'event_type' => $eventType,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
