<?php

use App\User;
use App\Models\Tag;
use Illuminate\Database\Seeder;

class FriendshipGraphSeeder extends Seeder
{
    public function run()
    {
        $eventlyFp = fopen(__DIR__ . '/../files/neo4j/evently.cypher', 'a+');

        // create user nodes
        $users = User::all();

        $usernames = [];
        $userNodes = [];
        $userNodeTemplate = "CREATE (%username%:Person {name:'%fullname%', born:%year%})";
        
        foreach ($users as $user) {
            $usernames[$user->id] = str_replace('.', '', $user->username);
            $userNodes[$user->id] = str_replace(
                ['%username%', '%fullname%', '%year%'],
                [str_replace('.', '', $user->username), $this->fullName($user), substr($user->date_of_birth, 0, 4)],
                $userNodeTemplate
            );
        }

        fwrite($eventlyFp, implode(PHP_EOL, $userNodes));
        fwrite($eventlyFp, PHP_EOL);
        fwrite($eventlyFp, PHP_EOL);

        // create interest nodes
        $userTags = Tag::whereRaw(['entityType' => 'User'])
            ->project(['_id' => 0, 'entityId' => 1, 'tags' => 1])
            ->get();

        $userTagRelationship = [];
        $allTags = [];
        foreach ($userTags as $userTag) {
            $userTagRelationship[$userTag->entityId] = $userTag->tags;
            $allTags = array_merge($allTags, $userTag->tags);
        }
        $allTags = array_values(array_unique($allTags));

        $interestNodes = [];
        $interestNodeTemplate = "CREATE (%tag%:Interest {name: '%tag%'})";

        foreach ($allTags as $tag) {
            $interestNodes[] = str_replace('%tag%', str_replace('-', '', $tag), $interestNodeTemplate);
        }

        fwrite($eventlyFp, implode(PHP_EOL, $interestNodes));
        fwrite($eventlyFp, PHP_EOL);
        fwrite($eventlyFp, PHP_EOL);

        // relate users to interests
        $interestedInRelationships = [];
        $interestedInRelationshipTemplate = "  (%person%)-[:INTERESTED_IN]->(%interest%)";

        foreach ($userTagRelationship as $userId => $tags) {
            foreach ($tags as $tag) {
                $interestedInRelationships[] = str_replace(
                    ['%person%', '%interest%'],
                    [$usernames[$userId], str_replace('-', '', $tag)],
                    $interestedInRelationshipTemplate
                );
            }
        }

        fwrite($eventlyFp, 'CREATE' . PHP_EOL);
        fwrite($eventlyFp, implode(',' . PHP_EOL, $interestedInRelationships));
        fwrite($eventlyFp, PHP_EOL);
        fwrite($eventlyFp, PHP_EOL);

        // relate users
        $maxFriendCount = 8;
        $userIds = array_keys($usernames);
        $userFriends = [];


        foreach ($userIds as $userId) {
            $otherUserIds = array_values(array_diff($userIds, [$userId]));
            $friendCount = mt_rand(0, $maxFriendCount);

            for ($i = 1; $i <= $friendCount; $i++) {
                $friendId = $otherUserIds[mt_rand(0, count($otherUserIds) - 1)];
                $otherUserIds = array_values(array_diff($otherUserIds, [$friendId]));
                $userFriends[$userId][] = $friendId;
            }
        }

        $friendsWithRelationships = [];
        $friendsWithRelationshipTemplate = "  (%person%)-[:FRIENDS_WITH]->(%friend%)";


        foreach ($userFriends as $userId => $friends) {
            foreach ($friends as $friend) {
                $friendsWithRelationships[] = str_replace(
                    ['%person%', '%friend%'],
                    [$usernames[$userId], $usernames[$friend]],
                    $friendsWithRelationshipTemplate
                );
            }
        }

        fwrite($eventlyFp, 'CREATE' . PHP_EOL);
        fwrite($eventlyFp, implode(',' . PHP_EOL, $friendsWithRelationships));
        fwrite($eventlyFp, PHP_EOL);
        
        
        fclose($eventlyFp);
    }

    private function fullName(User $user)
    {
        return $user->first_name . ' ' . $user->last_name;
    }
}
