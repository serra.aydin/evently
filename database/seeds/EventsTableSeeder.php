<?php

use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventsTableSeeder extends Seeder
{
    private $eventData = [];

    public function run()
    {
        $this->populateEvents();
        $eventCount = count($this->eventData);

        for ($i = 0; $i < $eventCount; $i++) {
            $this->getEvent($i)->save();
        }
    }

    /**
     * @param int $index
     * @return Event
     */
    private function getEvent($index)
    {
        $event = factory(Event::class)->make();

        $currentEventData = $this->eventData[$index];

        $event->event_type_id = $currentEventData['type'];
        $event->event_name = $currentEventData['name'];
        $event->event_date = $currentEventData['date'];

        return $event;
    }

    private function populateEvents()
    {
        $eventsCsvFilePath = database_path() . '/files/events.csv';
        $trimmed = file($eventsCsvFilePath, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        foreach ($trimmed as $line) {
            $parts = explode(',', $line);
            $this->eventData[] = [
                'type' => $parts[0],
                'name' => $parts[1],
                'date' => $this->populateDate()
            ];
        }
    }

    private function populateDate()
    {
        $timeZone = 'Europe/Istanbul';
        $startDate = Carbon::create(2018, 10, 30, 19, 0, 0, $timeZone);

        $dayAddition = mt_rand(0, 21);
        $hourAddition = mt_rand(0, 3);

        $startDate->addDays($dayAddition)->addHours($hourAddition);

        return $startDate->format('Y-m-d H:i:s');
    }
}
