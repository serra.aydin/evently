<?php

use App\Models\Venue;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class VenueHallsTableSeeder extends Seeder
{
    public function run()
    {
        $venues = Venue::all();

        foreach ($venues as $venue) {
            $venuHallCount = mt_rand(1, 4);

            for ($i = 1; $i <= $venuHallCount; $i++) {
                DB::table('venue_halls')->insert([
                    'venue_id' => $venue->id,
                    'vanue_hall_name' => 'Salon ' . $i,
                    'capacity' => mt_rand(0, 50) * 5,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ]);
            }
        }
    }
}
