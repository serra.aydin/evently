<?php
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserBirthDateSeeder extends Seeder
{
    public function run()
    {
        $users = User::all();

        foreach($users as $user){
            $user->date_of_birth = $this->populateDate();
            $user->save();
        }
    }

    private function populateDate()
    {
        $timeZone = 'Europe/Istanbul';
        $startDate = Carbon::create(1970, 1, 1, 0, 0, 0, $timeZone);

        $dayAddition = mt_rand(0, 28);
        $monthAddition = mt_rand(0,12);
        $yearAddition = mt_rand(0,50);

        $startDate->addYears($yearAddition)->addMonths($monthAddition)->addDays($dayAddition);

        return $startDate->format('Y-m-d H:i:s');
    }
}
