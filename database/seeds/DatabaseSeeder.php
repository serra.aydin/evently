<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(EventTypesTableSeeder::class);
        $this->call(VenuesTableSeeder::class);
        $this->call(VenueHallsTableSeeder::class);
        $this->call(EventsTableSeeder::class);
        $this->call(EventTicketsTableSeeder::class);
        $this->call(TicketOrdersTableSeeder::class);
    }
}
