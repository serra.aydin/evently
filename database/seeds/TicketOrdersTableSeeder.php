<?php

use App\Models\Event;
use App\Models\EventTicket;
use App\Models\TicketOrder;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TicketOrdersTableSeeder extends Seeder
{
    private $userIdsCache = [];

    public function run()
    {
        $events = Event::all();
        $this->userIdsCache = User::pluck('id')->all();

        foreach ($events as $event) {
            $eventDate = $event->event_date;
            $tickets = $event->tickets;

            foreach ($tickets as $ticket) {
                $soldTicketCount = $this->generateSoldTicketCount($ticket, $eventDate);

                for ($i = 0; $i < $soldTicketCount; $i++) {
                    TicketOrder::create([
                        'event_ticket_id' => $ticket->id,
                        'user_id' => $this->pickUserId(),
                    ]);
                }
            }
        }
    }

    /**
     * @param EventTicket $ticket
     * @param string $eventDateStr
     * @return int
     */
    public function generateSoldTicketCount(EventTicket $ticket, $eventDateStr)
    {
        $soldTicketPercentage = 0;

        $timezone = 'Europe/Istanbul';
        $eventDate = Carbon::createFromFormat('Y-m-d H:i:s', $eventDateStr);

        if ($eventDate->isPast()) {
            $soldTicketPercentage = mt_rand(90, 100);
        } else {
            $now = Carbon::now($timezone);
            $soldTicketPercentage = mt_rand(0, 90 - $eventDate->diffInDays($now) * 5);
        }

        return floor($ticket->capacity * $soldTicketPercentage / 100);
    }

    /**
     * @return int
     */
    public function pickUserId()
    {
        $userIndex = mt_rand(0, count($this->userIdsCache) - 1);
        return $this->userIdsCache[$userIndex];
    }
}
