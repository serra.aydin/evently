<?php

use App\Models\Event;
use App\Models\EventTicket;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventTicketsTableSeeder extends Seeder
{
    public function run()
    {
        $ticketCapacityRatios = [
            1 => [1],
            2 => [0.6, 0.4],
            3 => [0.6, 0.2, 0.2],
        ];

        $events = Event::all();

        foreach ($events as $event) {
            $randomTicketCount = mt_rand(1, 3);
            $venueHallCapacity = $event->venueHall->capacity;

            for ($i = 0; $i < $randomTicketCount; $i++) {
                $eventTicket = factory(EventTicket::class)->make();
                $eventTicket->event_id = $event->id;
                $eventTicket->capacity = $venueHallCapacity * $ticketCapacityRatios[$randomTicketCount][$i];
                $eventTicket->save();
            }
        }
    }
}
