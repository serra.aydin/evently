<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class VenuesTableSeeder extends Seeder
{
    public function run()
    {
        $venues = $this->getVenues();

        foreach ($venues as $venue) {
            DB::table('venues')->insert([
                'venue_name' => $venue['venue_name'],
                'display_name' => $venue['display_name'],
                'address' => $venue['address'],
                'city' => $venue['city'],
                'country' => 'Turkey',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
            ]);
        }
    }

    private function getVenues()
    {
        return [
            [
                'venue_name' => 'uniq-hall',
                'display_name' => 'UNIQ Hall',
                'address' => 'Sarıyer',
                'city' => 'İstanbul',
            ], [
                'venue_name' => 'yapi-kredi-kultur-sanat-loca',
                'display_name' => 'Yapı Kredi Kültür Sanat - LOCA',
                'address' => 'Beyoğlu',
                'city' => 'İstanbul',
            ], [
                'venue_name' => 'crr',
                'display_name' => 'CRR Konser Salonu',
                'address' => 'Harbiye',
                'city' => 'İstanbul',
            ], [
                'venue_name' => 'entropi-sahne',
                'display_name' => 'Entropi Sahne',
                'address' => 'Kadıköy',
                'city' => 'İstanbul',
            ],
            [
                'venue_name' => 'zorlu-psm',
                'display_name' => 'Zorlu PSM',
                'address' => 'Beşiktaş',
                'city' => 'İstanbul',
            ], [
                'venue_name' => 'salon-iksv',
                'display_name' => 'Salon İKSV',
                'address' => 'Şişhane',
                'city' => 'İstanbul',
            ], [
                'venue_name' => 'elite-world-istanbul-hotel-jazz-company',
                'display_name' => 'Jazz Company',
                'address' => 'Taksim',
                'city' => 'İstanbul',
            ], [
                'venue_name' => 'if-performance-hall-atasehir',
                'display_name' => 'IF Performance Hall',
                'address' => 'Ataşehir',
                'city' => 'İstanbul',
            ],
            [
                'venue_name' => 'moda-kayikhane',
                'display_name' => 'Moda Kayıkhane',
                'address' => 'Moda',
                'city' => 'İstanbul',
            ], [
                'venue_name' => 'oyun-atolyesi',
                'display_name' => 'Oyun Atölyesi',
                'address' => 'Moda',
                'city' => 'İstanbul',
            ], [
                'venue_name' => 'bkm-mutfak',
                'display_name' => 'BKM Mutfak',
                'address' => 'Beşiktaş',
                'city' => 'İstanbul',
            ], [
                'venue_name' => 'yildiz-kenter-tiyatro-salonu',
                'display_name' => 'Yıldız Kenter Tiyatro Salonu',
                'address' => 'Yenimahalle',
                'city' => 'Ankara',
            ],
            [
                'venue_name' => 'if-performance-hall-ankara',
                'display_name' => 'IF Performance Hall',
                'address' => 'Kavaklıdere',
                'city' => 'Ankara',
            ], [
                'venue_name' => 'odtü-salonu',
                'display_name' => 'ODTÜ Kültür ve Kongre Merkezi',
                'address' => 'Balgat',
                'city' => 'Ankara',
            ], [
                'venue_name' => 'izmir-sanat',
                'display_name' => 'İzmir Sanat',
                'address' => 'Merkez',
                'city' => 'İzmir',
            ], [
                'venue_name' => '6-45-kk-bornova',
                'display_name' => '6:45 KK Bornova',
                'address' => 'Bornova',
                'city' => 'İzmir',
            ],
            [
                'venue_name' => '232-park-bornova-sahne',
                'display_name' => '232 Park Bornova Sahne',
                'address' => 'Bornova',
                'city' => 'İzmir',
            ], [
                'venue_name' => '6-45-kk-adana',
                'display_name' => '6:45 KK Adana',
                'address' => 'Seyhane',
                'city' => 'Adana',
            ], [
                'venue_name' => 'hayal-kahvesi-adana',
                'display_name' => 'Hayal Kahvesi Adana',
                'address' => 'Merkez',
                'city' => 'Adana',
            ], [
                'venue_name' => 'adiyaman-universitesi',
                'display_name' => 'Adıyaman Üniversitesi',
                'address' => 'Merkez',
                'city' => 'Adıyaman',
            ],
            [
                'venue_name' => 'aspendos-arena',
                'display_name' => 'Aspendos Arena',
                'address' => 'Serik',
                'city' => 'Antalya',
            ],
        ];
    }
}
