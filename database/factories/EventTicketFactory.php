<?php

use App\Models\EventTicket;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(EventTicket::class, function (Faker $faker) {
    $ticketNames = [
        'Öğrenci', 'Tam', 'Sahne Önü', 'Balkon',
        'Salon A Blok', 'Salon B Blok', 'Salon C Blok'
    ];

    $ticketName = $ticketNames[mt_rand(0, count($ticketNames) - 1)];

    return [
        'ticket_name' => $ticketName,
    ];
});
