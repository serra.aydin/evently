<?php

use App\Models\Event;
use App\Models\EventType;
use App\Models\VenueHall;
use Faker\Generator as Faker;

$factory->define(Event::class, function (Faker $faker) {
    $venueHallCount = VenueHall::count();
    $randomVenueHall = VenueHall::find(mt_rand(1, $venueHallCount));

    return [
        'venue_hall_id' => $randomVenueHall->id,
        'duration_in_minutes' => (mt_rand(0, 4) * 30) + 60,
    ];
});
