@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="mb-4">
                <div class="d-flex justify-content-between">
                    <div><a id="prev" role="button" class="btn btn-primary" href="{{ route('events.index') }}">Prev</a></div>
                    <div><a id="next" role="button" class="btn btn-primary" href="{{ route('charts.event_count_by_tags') }}">Next</a></div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">Venues<div style="float:right">Total Page Views: {{ $pageVisitCount }} </div></div>

                <div class="card-body">
                    <button id="show-queries" type="button" class="btn btn-primary">Show Queries</button>

                    <code id="show-queries-code" class="d-none">
                    @foreach ($venuesPageResponse->getQueries() as $db => $query)
                        <br/><br/>
                        {{$db}}:
                        <br/>
                        {!! $query !!}
                    @endforeach
                    </code>

                    <table class="table mt-4">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Address</th>
                                <th>City</th>
                                <th>Hall Count</th>
                                <th>Tags</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($venuesPageResponse->getVenues() as $venue)
                            <tr>
                                <td>{{ $venue->id }}</td>
                                <td>{{ $venue->display_name }}</td>
                                <td>{{ $venue->address }}</td>
                                <td>{{ $venue->city }}</td>
                                <td>{{ $venue->hallCount }}</td>
                                <td>{{ $venue->tags }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
