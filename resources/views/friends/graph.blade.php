@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="mb-4">
                <div class="d-flex justify-content-between">
                    <div><a id="prev" role="button" class="btn btn-primary" href="{{ route('documents.index') }}">Prev</a></div>
                    <div><a id="next" role="button" class="btn btn-primary" href="{{ route('users.index') }}">Next</a></div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">Graph Diagram</div>

                <div class="card-body">
                    <img src={{URL::asset('/img/evently-graph-diagram.png')}} />
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
