@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="mb-4">
                <div class="d-flex justify-content-between">
                    <div><a id="prev" role="button" class="btn btn-primary" href="{{ route('charts.event_counts_by_day') }}">Prev</a></div>
                    <div><a id="next" role="button" class="btn btn-primary" href="{{ route('home') }}">Next</a></div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">Friends' Interests</div>

                <div class="card-body">
                    <button id="show-queries" type="button" class="btn btn-primary">Show Queries</button>

                    <code id="show-queries-code" class="d-none">
                    @foreach ($queries as $db => $query)
                        <br/><br/>
                        {{$db}}:
                        <br/>
                        {!! $query !!}
                    @endforeach
                    </code>

                    <table class="table mt-4">
                        <thead>
                            <tr>
                                <th>Friend</th>
                                <th>Interests</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if (!empty($friends))
                                @foreach ($friends as $friend)
                                <tr>
                                    <td>{{ $friend['name'] }}</td>
                                    <td>{{ $friend['interests'] }}</td>
                                </tr>
                                @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
