@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="mb-4">
                <div class="d-flex justify-content-between">
                    <div><a id="prev" role="button" class="btn btn-primary" href="{{ route('friends.interests') }}">Prev</a></div>
                    <div><a id="next" role="button" class="btn btn-primary" href="{{ route('documents.index') }}">Next</a></div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">ER Diagram</div>

                <div class="card-body d-flex justify-content-center">
                    <img src="img/evently-rdbms-er-diagram.png" />
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
