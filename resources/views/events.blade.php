@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="mb-4">
                <div class="d-flex justify-content-between">
                    <div><a id="prev" role="button" class="btn btn-primary" href="{{ route('users.index') }}">Prev</a></div>
                    <div><a id="next" role="button" class="btn btn-primary" href="{{ route('venues.index') }}">Next</a></div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">Events<div style="float:right">Total Page Views: {{ $pageVisitCount }} </div></div>

                <div class="card-body">
                    <button id="show-queries" type="button" class="btn btn-primary">Show Queries</button>

                    <code id="show-queries-code" class="d-none">
                    @foreach ($eventsPageResponse->getQueries() as $db => $query)
                        <br/><br/>
                        {{$db}}:
                        <br/>
                        {!! $query !!}
                    @endforeach
                    </code>

                    <table class="table mt-4">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Type</th>
                                <th style="width: 20%;">Name</th>
                                <th>Venue</th>
                                <th>Hall</th>
                                <th>Date</th>
                                <th>Duration</th>
                                <th>Capacity</th>
                                <th>Remaining</th>
                                <th>Tags</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($eventsPageResponse->getEvents() as $event)
                            <tr>
                                <td>{{ $event->id }}</td>
                                <td>{{ ucfirst($event->event_type) }}</td>
                                <td>{{ $event->event_name }}</td>
                                <td>{{ $event->display_name }}</td>
                                <td>{{ $event->venue_hall_name }}</td>
                                <td>{{ $event->event_date }}</td>
                                <td>{{ $event->duration_in_minutes }}</td>
                                <td>{{ $event->capacity }}</td>
                                <td>{{ $event->remainingTicket }}</td>
                                <td>
                                    <a href="javascript:;" data-toggle="tooltip" data-placement="bottom"
                                       title="{{ $event->tags }}">Tags</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
