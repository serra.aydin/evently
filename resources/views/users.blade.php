@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
        <div class="mb-4">
                <div class="d-flex justify-content-between">
                    <div><a id="prev" role="button" class="btn btn-primary" href="{{ route('friends.graph') }}">Prev</a></div>
                    <div><a id="next" role="button" class="btn btn-primary" href="{{ route('events.index') }}">Next</a></div>
                </div>
            </div>

            <div class="card">
                <div class="card-header">Users<div style="float:right">Total Page Views: {{ $pageVisitCount }} </div></div>
                <div class="card-body">
                    <button id="show-queries" type="button" class="btn btn-primary">Show Queries</button>

                    <code id="show-queries-code" class="d-none">
                    @foreach ($userPageQueries as $db => $query)
                        <br/><br/>
                        {{$db}}:
                        <br/>
                        {!! $query !!}
                    @endforeach
                    </code>

                    <table class="table mt-4">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Age</th>
                                <th>Tags</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td style="width: 8%;">{{ $user->first_name }}</td>
                                <td style="width: 8%;">{{ $user->last_name }}</td>
                                <td style="width: 15%;">{{ $user->username }}</td>
                                <td style="width: 12%;">{{ $user->email }}</td>
                                <td style="width: 8%;"><?php  echo Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $user->date_of_birth)->age; ?></td>
                                <td>
                                    <?php
                                        if(isset($userTags[$user->id])){
                                            echo implode(', ', $userTags[$user->id]);
                                        }
                                    ?>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
