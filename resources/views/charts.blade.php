@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="mb-4">
                <div class="d-flex justify-content-between">
                    <div><a id="prev" role="button" class="btn btn-primary" href="{{ $prevLink }}">Prev</a></div>
                    <div><a id="next" role="button" class="btn btn-primary" href="{{ $nextLink }}">Next</a></div>
                </div>
            </div>
            
            <div class="card">
                <div class="card-header">{{ $header }}</div>
                <div class="card-body">
                <button id="show-queries" type="button" class="btn btn-primary">Show Queries</button>

                    <code id="show-queries-code" class="d-none">
                    @foreach ($queries as $type => $query)
                        <br/><br/>
                        {{ $type }}:
                        <br/>       
                        {!! $query !!}
                    @endforeach
                    </code>

                    <div style="width:75%; margin-top:20px">
                            {!! $chartjs->render() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
