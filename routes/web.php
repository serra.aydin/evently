<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/documents', 'HomeController@document')->name('documents.index');
Route::resource('users', 'UserController');
Route::get('/events', 'EventController@index')->name('events.index');
Route::get('/venues', 'VenueController@index')->name('venues.index');
Route::get('/charts/event-count-by-tags', 'ChartController@drawEventCountByTags')->name('charts.event_count_by_tags');
Route::get('/charts/event-participation-by-age-group', 'ChartController@drawEventParticipationByAgeGroup')->name('charts.event_participation');
Route::get('/charts/event-counts-by-week', 'ChartController@drawEventCountsByWeek')->name('charts.event_counts_by_week');
Route::get('/charts/event-counts-by-day', 'ChartController@drawEventCountsByDay')->name('charts.event_counts_by_day');
Route::get('/logout', 'LoginController@logout');
Route::get('/friends/graph', 'FriendsController@graph')->name('friends.graph');
Route::get('/friends/interests', 'FriendsController@interests')->name('friends.interests');
