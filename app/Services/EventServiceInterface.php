<?php

namespace App\Services;

interface EventServiceInterface
{
    public function getEventsPageResponse();
}
