<?php

namespace App\Services;

use App\Http\Response\VenuesPageResponse;
use App\Models\Venue;
use App\Repositories\TagRepositoryInterface;
use App\Repositories\TagRepository;
use App\Repositories\VenueRepositoryInterface;
use App\Repositories\VenueRepository;

class VenueService implements VenueServiceInterface
{
    /** @var VenueRepositoryInterface $venueRepository */
    protected $venueRepository;
    /** @var TagRepositoryInterface $tagRepository */
    protected $tagRepository;

    public function __construct(
        VenueRepositoryInterface $venueRepository,
        TagRepositoryInterface $tagRepository
    )
    {
        $this->venueRepository = $venueRepository;
        $this->tagRepository = $tagRepository;
    }

    /**
     * @return VenuesPageResponse
     */
    public function getVenuesPageResponse()
    {
        $venues = $this->venueRepository->getVenues();
        $venueHallCounts = $this->venueRepository->getVenueHallCounts();
        $venueTags = $this->tagRepository->getEntityTags(class_basename(Venue::class));

        foreach ($venues as $key => $venue) {
            $venues[$key]->hallCount = $venueHallCounts[$venue->id];

            $tags = [];
            if (isset($venueTags[$venue->id])) {
                $tags = $venueTags[$venue->id];
            }

            $venues[$key]->tags = implode(', ', $tags);
        }

        $venuesPageResponse = new VenuesPageResponse();
        $venuesPageResponse->setVenues($venues);
        $venuesPageResponse->setQueries([
            'MySQL #1' => nl2br(VenueRepository::VENUE_LISTING_QUERY),
            'MySQL #2' => nl2br(VenueRepository::VENUE_HALL_COUNT_QUERY),
            'MongoDB' => VenueRepository::VENUE_TAGS_QUERY,
            'Redis' => TagRepository::VENUE_INCR_QUERY
        ]);

        return $venuesPageResponse;
    }
}
