<?php

namespace App\Services;

interface VenueServiceInterface
{
    public function getVenuesPageResponse();
}
