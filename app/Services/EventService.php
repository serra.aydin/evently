<?php

namespace App\Services;

use App\Http\Response\EventsPageResponse;
use App\Repositories\EventRepository;
use App\Repositories\EventRepositoryInterface;
use App\Repositories\TagRepository;
use App\Repositories\TagRepositoryInterface;

class EventService implements EventServiceInterface
{
    /** @var EventRepositoryInterface */
    protected $eventRepository;
    /** @var TagRepositoryInterface $tagRepository */
    protected $tagRepository;

    /**
     * @param EventRepositoryInterface $eventRepository
     */
    public function __construct(
        EventRepositoryInterface $eventRepository,
        TagRepositoryInterface $tagRepository
    ) {
        $this->eventRepository = $eventRepository;
        $this->tagRepository = $tagRepository;
    }

    /**
     * return EventPageResponse
     */
    public function getEventsPageResponse()
    {
        $events = $this->eventRepository->getEvents();
        $soldTicketCounts = $this->eventRepository->getSoldTicketCounts();
        $eventTags = $this->tagRepository->getEntityTags(class_basename(Event::class));

        foreach ($events as $key => $event) {
            $remainingTicket = $event->capacity - $soldTicketCounts[$event->id];
            $events[$key]->remainingTicket = $remainingTicket > 0 ? $remainingTicket : "Sold Out";

            $tags = [];
            if (isset($eventTags[$event->id])) {
                $tags = $eventTags[$event->id];
            }

            $events[$key]->tags = implode(PHP_EOL, $tags);

            if (empty($events[$key]->tags)) {
                $events[$key]->tags = 'Not exist';
            }
        }

        $eventsPageResponse = new EventsPageResponse();
        $eventsPageResponse->setEvents($events);
        $eventsPageResponse->setQueries([
            'MySQL #1' => nl2br(EventRepository::EVENT_LISTING_QUERY),
            'MySQL #2' => nl2br(EventRepository::EVENT_SOLD_TICKET_COUNT_QUERY),
            'MongoDB' => EventRepository::EVENT_TAGS_QUERY,
            'Redis' => TagRepository::EVENT_INCR_QUERY,
        ]);

        return $eventsPageResponse;
    }
}
