<?php

namespace App\Services;

use GraphAware\Neo4j\Client\ClientBuilder as Neo4jClientBuilder;
use GraphAware\Bolt\Configuration;
use GraphAware\Bolt\GraphDatabase;
use Illuminate\Support\Facades\App;

class FriendService implements FriendServiceInterface
{
    const FRIENDS_INTERESTS_CYPHER_QUERY = 
        "MATCH (p:Person {name: '%user_full_name%'})-[:FRIENDS_WITH]->(f:Person)-[:INTERESTED_IN]->(i:Interest)
        RETURN f.name AS friend_name, collect(i.name) AS interests";

    public function getInterestsByUser($userFullName)
    {
        $neo4jConfig = config('database.connections.neo4j');

        $config = Configuration::newInstance()
            ->withCredentials($neo4jConfig['bolt_user'], $neo4jConfig['bolt_password'])
            ->withTimeout(10);

        if ("prod" == App::environment()) {
            $config = $config->withTLSMode(Configuration::TLSMODE_REQUIRED);
        }

        $driver = GraphDatabase::driver($neo4jConfig['bolt_url'], $config);
        $client = $driver->session();

        $query = str_replace('%user_full_name%', $userFullName, static::FRIENDS_INTERESTS_CYPHER_QUERY);
        $result = $client->run($query);

        $friends = [];
        foreach ($result->getRecords() as $record) {
            $friends[] = [
                'name' => $record->value('friend_name'),
                'interests' => implode(', ', $record->value('interests'))
            ];
        }

        return $friends;
    }
}
