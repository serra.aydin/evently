<?php

namespace App\Services;

interface FriendServiceInterface
{
    public function getInterestsByUser($userId);
}
