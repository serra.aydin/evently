<?php

namespace App\Services;

use App\Http\Response\ChartsPageResponse;
use App\Repositories\EventRepository;
use App\Repositories\EventRepositoryInterface;
use App\Repositories\TagRepository;
use App\Repositories\TagRepositoryInterface;
use Illuminate\Support\Facades\DB;

class ChartService implements ChartServiceInterface
{
    const USER_EVENT_HISTOGRAM_QUERY =
        "SELECT FLOOR(((YEAR(NOW()) - YEAR(u.date_of_birth)) - ((DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(u.date_of_birth, '00-%m-%d')))) / 10) AS age_group, COUNT(tor.id) AS event_count
        FROM users u
        INNER JOIN ticket_orders tor ON tor.user_id = u.id
        GROUP BY age_group";

    const EVENT_COUNTS_BY_WEEK_QUERY =
        "SELECT WEEK(event_date) AS week_of_event, COUNT(*) AS event_count
        FROM events
        GROUP BY week_of_event";

    const EVENT_COUNTS_BY_DAY_QUERY =
        "SELECT DATE_FORMAT(event_date, '%m.%d') AS event_day, COUNT(*) AS event_count
        FROM events
        GROUP BY event_day";

    /** @var EventRepositoryInterface */
    protected $eventRepository;
    /** @var TagRepositoryInterface $tagRepository */
    protected $tagRepository;

    /**
     * @param EventRepositoryInterface $eventRepository
     */
    public function __construct(
        EventRepositoryInterface $eventRepository,
        TagRepositoryInterface $tagRepository
    ) {
        $this->eventRepository = $eventRepository;
        $this->tagRepository = $tagRepository;
    }

    public function getEventParticipationData()
    {
        $participatedEventCount = [];
        $ageGroups = [
            1 => "10-19 years old",
            2 => "20-29 years old",
            3 => "30-39 years old",
            4 => "40-49 years old",
        ];

        $result = DB::select(static::USER_EVENT_HISTOGRAM_QUERY);

        foreach ($result as $row) {
            $participatedEventCount[$ageGroups[$row->age_group]]
            = $row->event_count;
        }

        return $participatedEventCount;
    }

    /**
     * return ChartsPageResponse
     */
    public function getEventTagsChartDetails()
    {
        $events = $this->eventRepository->getEvents();
        $eventTags = $this->tagRepository->getEntityTags(class_basename(Event::class));

        $tags = [];
        foreach ($events as $key => $event) {
            if (isset($eventTags[$event->id])) {
                foreach ($eventTags[$event->id] as $eventTag) {
                    if (isset($tags[$eventTag])) {
                        $tags[$eventTag]++;
                    } else {
                        $tags[$eventTag] = 0;
                    }
                }
            }
        }

        $chartsPageResponse = new ChartsPageResponse();
        $chartsPageResponse->setEventTags($tags);

        return $chartsPageResponse;
    }

    public function getEventCountsByWeekData()
    {
        $eventCountsByWeek = [];

        $result = DB::select(static::EVENT_COUNTS_BY_WEEK_QUERY);

        foreach ($result as $row) {
            $eventCountsByWeek['Week #' . $row->week_of_event] = $row->event_count;
        }

        return $eventCountsByWeek;
    }

    public function getEventCountsByDayData()
    {
        $eventCountsByDay = [];
        $months = ['10' => 'Oct.', '11' => 'Nov.'];

        $result = DB::select(static::EVENT_COUNTS_BY_DAY_QUERY);

        foreach ($result as $row) {
            $eventDayParts = explode('.', $row->event_day);
            $eventDayKey = $months[$eventDayParts[0]] . ' ' . intval($eventDayParts[1]);

            $eventCountsByDay[$eventDayKey] = $row->event_count;
        }

        return $eventCountsByDay;
    }
}
