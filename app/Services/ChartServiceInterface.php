<?php

namespace App\Services;

interface ChartServiceInterface
{
    public function getEventParticipationData();
    public function getEventTagsChartDetails();
    public function getEventCountsByWeekData();
    public function getEventCountsByDayData();
}
