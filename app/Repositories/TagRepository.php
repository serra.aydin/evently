<?php namespace App\Repositories;

use App\Models\Tag;

class TagRepository implements TagRepositoryInterface
{
    const VENUE_INCR_QUERY = "INCR venuesVisit:count";
    const USER_INCR_QUERY = "INCR usersVisit:count";
    const EVENT_INCR_QUERY = "INCR eventsVisit:count";

    protected $tag;

    public function setTagModel(Tag $tag)
    {
        $this->tag = $tag;
    }

    public function all()
    {
        return $this->tag::all();
    }

    public function getEntityTags($entityName)
    {
        $tags = [];
        $entityTags = Tag::whereRaw(['entityType' => $entityName])->project(['_id' => 0, 'entityId' => 1, 'tags' => 1])->get();

        foreach ($entityTags as $entityTag) {
            $tags[$entityTag->entityId] = $entityTag->tags;
        }

        return $tags;
    }

    public function getEntityInstanceTags($entityName, $id)
    {
        $tags = [];
        $entityTags = Tag::whereRaw(['entityType' => $entityName, 'entityId' => $id])->project(['_id' => 0, 'entityId' => 1, 'tags' => 1])->get();

        foreach ($entityTags as $entityTag) {
            $tags[$entityTag->entityId] = $entityTag->tags;
        }

        return $tags;
    }
}
