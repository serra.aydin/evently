<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;

class VenueRepository implements VenueRepositoryInterface
{
    const VENUE_LISTING_QUERY = 
        "SELECT v.id, v.display_name, v.address, v.city
        FROM venues v";

    const VENUE_HALL_COUNT_QUERY =
        "SELECT v.id, COUNT(*) AS hall_count
        FROM venue_halls vh
        INNER JOIN venues v ON v.id = vh.venue_id
        GROUP BY v.id";

    const VENUE_TAGS_QUERY = 
        "db.tags.find({\"entityType\": \"Venue\"}, {\"_id\": 0, \"entityId\": 1, \"tags\": 1})";

    /**
     * @return array
     */
    public function getVenues()
    {
        return DB::select(static::VENUE_LISTING_QUERY);
    }

    /**
     * @return array
     */
    public function getVenueHallCounts()
    {
        $result = DB::select(static::VENUE_HALL_COUNT_QUERY);

        $venueHallCounts = [];
        foreach ($result as $row) {
            $venueHallCounts[$row->id] = $row->hall_count;
        }

        return $venueHallCounts;
    }
}
