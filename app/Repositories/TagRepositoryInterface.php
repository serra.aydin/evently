<?php namespace App\Repositories;

interface TagRepositoryInterface
{
    public function all();

    public function getEntityTags($entityName);

    public function getEntityInstanceTags($entityName, $id);
}
