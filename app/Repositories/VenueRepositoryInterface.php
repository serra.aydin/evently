<?php

namespace App\Repositories;

interface VenueRepositoryInterface
{
    public function getVenues();
    public function getVenueHallCounts();
}
