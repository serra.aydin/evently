<?php namespace App\Repositories;

use Illuminate\Support\Facades\Redis;

class RedisRepository
{
    public function incr($key)
    {
        return Redis::incr($key);
    }

    public function decr($key)
    {
        return Redis::decr($key);
    }

    public function get($key)
    {
        return Redis::get($key);
    }

    public function set($key)
    {
        return Redis::set($key);
    }

    public function del($key)
    {
        return Redis::del($key);
    }
}
