<?php

namespace App\Repositories;

interface EventRepositoryInterface
{
    public function getEvents();
    public function getSoldTicketCounts();
}
