<?php

namespace App\Repositories;

use Illuminate\Support\Facades\DB;

class EventRepository implements EventRepositoryInterface
{
    const EVENT_LISTING_QUERY = 
        "SELECT e.id, et.event_type, e.event_name, v.display_name, 
        vh.venue_hall_name, vh.capacity, e.event_date, e.duration_in_minutes 
        FROM events e 
        INNER JOIN event_types et ON et.id = e.event_type_id 
        INNER JOIN venue_halls vh ON vh.id = e.venue_hall_id 
        INNER JOIN venues v ON v.id = vh.venue_id 
        ORDER BY e.event_date DESC";

    const EVENT_SOLD_TICKET_COUNT_QUERY = 
        "SELECT e.id, COUNT(tor.id) AS tickets_sold
        FROM ticket_orders tor
        RIGHT JOIN event_tickets et ON et.id = tor.event_ticket_id
        INNER JOIN events e ON e.id = et.event_id
        GROUP BY e.id";

    const EVENT_TAGS_QUERY = 
        "db.tags.find({\"entityType\": \"Event\"}, {\"_id\": 0, \"entityId\": 1, \"tags\": 1})";

    /**
     * @return array
     */
    public function getEvents()
    {
        return DB::select(static::EVENT_LISTING_QUERY);
    }

    /**
     * @return array
     */
    public function getSoldTicketCounts()
    {
        $result = DB::select(static::EVENT_SOLD_TICKET_COUNT_QUERY);

        $soldTicketCounts = [];
        foreach ($result as $row) {
            $soldTicketCounts[$row->id] = $row->tickets_sold;
        }

        return $soldTicketCounts;
    }
}
