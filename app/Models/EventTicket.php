<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventTicket extends Model
{
    protected $fillable = ['event_id', 'ticket_name', 'capacity', 'price'];
}
