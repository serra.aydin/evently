<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = [
        'event_name', 'event_type_id', 'venue_hall_id', 'event_date', 'duration_in_minutes',
    ];

    public function venueHall()
    {
        return $this->belongsTo(VenueHall::class);
    }

    public function tickets()
    {
        return $this->hasMany(EventTicket::class);
    }
}
