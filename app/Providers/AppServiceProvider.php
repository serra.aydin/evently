<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\EventRepositoryInterface;
use App\Repositories\EventRepository;
use App\Repositories\TagRepositoryInterface;
use App\Repositories\TagRepository;
use App\Repositories\VenueRepositoryInterface;
use App\Repositories\VenueRepository;
use App\Services\EventServiceInterface;
use App\Services\EventService;
use App\Services\ChartServiceInterface;
use App\Services\ChartService;
use App\Services\VenueServiceInterface;
use App\Services\VenueService;
use App\Services\FriendServiceInterface;
use App\Services\FriendService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(\Faker\Generator::class, function () {
            return \Faker\Factory::create('tr_TR');
        });

        $this->app->bind(EventRepositoryInterface::class, EventRepository::class);
        $this->app->bind(EventServiceInterface::class, EventService::class);
        $this->app->bind(ChartServiceInterface::class, ChartService::class);
        $this->app->bind(VenueRepositoryInterface::class, VenueRepository::class);
        $this->app->bind(VenueServiceInterface::class, VenueService::class);
        $this->app->bind(TagRepositoryInterface::class, TagRepository::class);
        $this->app->bind(FriendServiceInterface::class, FriendService::class);
    }
}
