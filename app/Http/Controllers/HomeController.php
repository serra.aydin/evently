<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Highlight\Highlighter;
use App\Models\Tag;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function document()
    {
        $sampleCode = $this->getMarkupHighlightedCode();
        return view('document', compact('sampleCode'));
    }

    private function getMarkupHighlightedCode()
    {
        $highlighter = new Highlighter();

        $language = "json";
        $code = file_get_contents(database_path() . '/tags_sample.json');

        return $highlighter->highlight($language, $code);
    }
}
