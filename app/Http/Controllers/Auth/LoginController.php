<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Repositories\RedisRepository;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    /** @var RedisRepository */
    protected $redis;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(RedisRepository $redisRepository)
    {
        $this->redis = $redisRepository;
        $this->middleware('guest')->except('logout');
    }

    public function logout()
    {
        //$this->redis->del('venuesVisit:count');
        //$this->redis->del('usersVisit:count');
        //$this->redis->del('eventsVisit:count');
        Auth::logout();
        return Redirect::to('login');
    }
}
