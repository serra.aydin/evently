<?php

namespace App\Http\Controllers;

use App\Models\Tag;
use App\Repositories\RedisRepository;
use App\Repositories\TagRepository;
use App\Repositories\TagRepositoryInterface;
use App\User;
use Illuminate\Support\Facades\Redis;

class UserController extends Controller
{
    const USER_TAGS_QUERY =
        "db.tags.find({\"entityType\": \"User\"}, {\"_id\": 0, \"entityId\": 1, \"tags\": 1})";

    /** @var TagServiceInterface */
    protected $tag;
    /** @var RedisRepository */
    protected $redis;

    /**
     * @param Tag $tag
     * @param TagRepositoryInterface $venueService
     * @param RedisRepository $redisRepository
     */
    public function __construct(Tag $tag, TagRepositoryInterface $tagRepository, RedisRepository $redisRepository)
    {
        $this->tag = $tagRepository;
        $this->tag->setTagModel($tag);
        $this->redis = $redisRepository;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->redis->incr('usersVisit:count');
        $pageVisitCount = $this->redis->get('usersVisit:count');
        $users = User::all();
        $userTags = $this->tag->getEntityTags(class_basename((User::class)));

        $userPageQueries = [
            'ORM' => 'User::all()',
            'MongoDB' => UserController::USER_TAGS_QUERY,
            'Redis' => TagRepository::USER_INCR_QUERY,
        ];

        return view('users', compact('users', 'userTags', 'pageVisitCount', 'userPageQueries'));
    }
}
