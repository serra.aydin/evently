<?php

namespace App\Http\Controllers;

use App\Repositories\RedisRepository;
use App\Services\EventServiceInterface;

class EventController extends Controller
{
    /** @var EventServiceInterface */
    protected $eventService;
    /** @var RedisRepository */
    protected $redis;

    /**
     * @param EventServiceInterface $eventService
     * @param RedisRepository $redisRepository
     */
    public function __construct(EventServiceInterface $eventService, RedisRepository $redisRepository)
    {
        $this->eventService = $eventService;
        $this->redis = $redisRepository;
        $this->middleware('auth');
    }

    public function index()
    {
        $this->redis->incr('eventsVisit:count');
        $pageVisitCount = $this->redis->get('eventsVisit:count');
        $eventsPageResponse = $this->eventService->getEventsPageResponse();
        return view('events', compact('eventsPageResponse', 'pageVisitCount'));
    }
}
