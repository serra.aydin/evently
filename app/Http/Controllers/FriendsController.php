<?php

namespace App\Http\Controllers;

use App\Services\FriendServiceInterface;
use App\Services\FriendService;

class FriendsController extends Controller
{
    /** @var FriendServiceInterface */
    private $friendService;

    /**
     * @param FriendServiceInterface $friendService
     */
    public function __construct(FriendServiceInterface $friendService)
    {
        $this->friendService = $friendService;
        $this->middleware('auth');
    }

    public function graph()
    {
        return view('friends.graph');
    }

    public function interests()
    {        
        $user = auth()->user();
        $userFullName = $user->first_name . ' ' . $user->last_name;

        $friends = $this->friendService->getInterestsByUser($userFullName);
        $queries['Neo4J'] = nl2br(FriendService::FRIENDS_INTERESTS_CYPHER_QUERY);

        return view('friends.interests', compact('friends', 'queries'));
    }
}
