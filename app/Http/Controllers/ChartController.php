<?php

namespace App\Http\Controllers;

use App\Repositories\EventRepository;
use App\Services\ChartService;
use App\Services\ChartServiceInterface;

class ChartController extends Controller
{
    /** @var ChartServiceInterface */
    protected $chartService;

    /**
     * @param ChartServiceInterface $chartService
     */
    public function __construct(ChartServiceInterface $chartService)
    {
        $this->chartService = $chartService;
        $this->middleware('auth');
    }

    public function drawEventCountByTags()
    {
        $eventTags = $this->chartService->getEventTagsChartDetails();
        
        $queries['MySQL'] = nl2br(EventRepository::EVENT_LISTING_QUERY);
        $queries['MongoDB'] = nl2br(EventRepository::EVENT_TAGS_QUERY);

        $colors = ['#003f5c', '#2f4b7c', '#665191', '#a05195', '#d45087',
            '#f95d6a', '#ff7c43', '#ffa600', '#ffc600', '#333399',
            '#009999', '#00cccc', '#33ccff', '#3399cc', '#336699'];

        $chartjs = app()->chartjs
            ->name('pieChartTest')
            ->type('doughnut')
            ->size(['width' => 400, 'height' => 300])
            ->labels(array_keys($eventTags->getEventTags()))
            ->datasets([
                [
                    'backgroundColor' => $colors,
                    'hoverBackgroundColor' => $colors,
                    'label' => 'Event Tags',
                    'data' => array_values($eventTags->getEventTags()),
                ],
            ])
            ->optionsRaw([
                'legend' => [
                    'display' => true,
                    'labels' => [
                        'fontColor' => '#000',
                    ],
                ]]);

        $prevLink = route('venues.index');
        $nextLink = route('charts.event_participation');
        $header = 'Event Counts by Tags';

        return view('charts', compact('chartjs', 'queries', 'prevLink', 'nextLink', 'header'));
    }

    public function drawEventParticipationByAgeGroup()
    {
        $eventParticipation = $this->chartService->getEventParticipationData();
        $queries['MySQL'] = nl2br(ChartService::USER_EVENT_HISTOGRAM_QUERY);

        $colors = ['#2f4b7c', '#665191', '#a05195', '#d45087'];

        $chartjs = app()->chartjs
            ->name('eventParticipationByAgeGroup')
            ->type('bar')
            ->size(['width' => 400, 'height' => 200])
            ->labels(array_keys($eventParticipation))
            ->datasets([
                [
                    'backgroundColor' => $colors,
                    'hoverBackgroundColor' => $colors,
                    'label' => 'Event Participation by Age Group',
                    'data' => array_values($eventParticipation),
                ],
            ])
            ->optionsRaw("{
                scales: {
                    xAxes: [{
                        gridLines: {
                            display:false
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display:false
                        }
                    }]
                }
            }");

        $prevLink = route('charts.event_count_by_tags');
        $nextLink = route('charts.event_counts_by_week');
        $header = 'Event Participation by Age Group';

        return view('charts', compact('chartjs', 'queries', 'prevLink', 'nextLink', 'header'));
    }

    public function drawEventCountsByWeek()
    {
        $eventCountsByWeek = $this->chartService->getEventCountsByWeekData();
        $queries['MySQL'] = nl2br(ChartService::EVENT_COUNTS_BY_WEEK_QUERY);

        $colors = ['#00cccc', '#33ccff', '#3399cc', '#336699'];

        $chartjs = app()->chartjs
            ->name('eventCountsByWeek')
            ->type('bar')
            ->size(['width' => 400, 'height' => 200])
            ->labels(array_keys($eventCountsByWeek))
            ->datasets([
                [
                    'backgroundColor' => $colors,
                    'hoverBackgroundColor' => $colors,
                    'label' => 'Event Counts by Week',
                    'data' => array_values($eventCountsByWeek),
                ],
            ])
            ->optionsRaw("{
                scales: {
                    xAxes: [{
                        gridLines: {
                            display:false
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display:false
                        }
                    }]
                }
            }");

        $prevLink = route('charts.event_participation');
        $nextLink = route('charts.event_counts_by_day');
        $header = 'Event Counts by Week';
    
        return view('charts', compact('chartjs', 'queries', 'prevLink', 'nextLink', 'header'));
    }

    public function drawEventCountsByDay()
    {
        $eventCountsByDay = $this->chartService->getEventCountsByDayData();
        $queries['MySQL'] = nl2br(ChartService::EVENT_COUNTS_BY_DAY_QUERY);

        $colors = ['#336699'];

        $chartjs = app()->chartjs
            ->name('eventCountsByDay')
            ->type('bar')
            ->size(['width' => 400, 'height' => 200])
            ->labels(array_keys($eventCountsByDay))
            ->datasets([
                [
                    'backgroundColor' => array_fill(0, count($eventCountsByDay), $colors),
                    'hoverBackgroundColor' => array_fill(0, count($eventCountsByDay), $colors),
                    'label' => 'Event Counts by Day',
                    'data' => array_values($eventCountsByDay),
                ],
            ])
            ->optionsRaw("{
                scales: {
                    xAxes: [{
                        gridLines: {
                            display:false
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display:false
                        },
                        ticks: {
                            beginAtZero: true,
                            min: 0
                        }
                    }]
                }
            }");

        $prevLink = route('charts.event_counts_by_week');
        $nextLink = route('friends.interests');
        $header = 'Event Counts by Day';

        return view('charts', compact('chartjs', 'queries', 'prevLink', 'nextLink', 'header'));
    }
}
