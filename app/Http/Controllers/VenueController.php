<?php

namespace App\Http\Controllers;

use App\Repositories\RedisRepository;
use App\Services\VenueServiceInterface;

class VenueController extends Controller
{
    /** @var VenueServiceInterface $venueService */
    protected $venueService;
    /** @var RedisRepository */
    protected $redis;

    /**
     * @param VenueServiceInterface $venueService
     * @param RedisRepository $redisRepository
     */
    public function __construct(VenueServiceInterface $venueService, RedisRepository $redisRepository)
    {
        $this->venueService = $venueService;
        $this->redis = $redisRepository;
        $this->middleware('auth');
    }

    public function index()
    {
        $this->redis->incr('venuesVisit:count');
        $pageVisitCount = $this->redis->get('venuesVisit:count');
        $venuesPageResponse = $this->venueService->getVenuesPageResponse();
        return view('venues', compact('venuesPageResponse', 'pageVisitCount'));
    }
}
