<?php

namespace App\Http\Response;

class VenuesPageResponse
{
    /** @var array */
    private $venues;
    /** @var array */
    private $queries;

    /**
     * @return array
     */
    public function getVenues()
    {
        return $this->venues;
    }

    /**
     * @param array $venues
     */
    public function setVenues($venues)
    {
        $this->venues = $venues;
    }

    /**
     * @return array
     */
    public function getQueries()
    {
        return $this->queries;
    }

    /**
     * @param array $queries
     */
    public function setQueries($queries)
    {
        $this->queries = $queries;
    }
}
