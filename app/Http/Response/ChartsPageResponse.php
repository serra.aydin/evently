<?php

namespace App\Http\Response;

class ChartsPageResponse
{
    /** @var array */
    private $eventTags;

    /**
     * @return array
     */
    public function getEventTags()
    {
        return $this->eventTags;
    }

    /**
     * @param array $eventTags
     */
    public function setEventTags($eventTags)
    {
        $this->eventTags = $eventTags;
    }
}
